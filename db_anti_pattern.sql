/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : db_anti_pattern

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-09-30 22:45:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `accounts`
-- ----------------------------
DROP TABLE IF EXISTS `accounts`;
CREATE TABLE `accounts` (
  `account_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `account_name` varchar(20) DEFAULT NULL,
  `first_name` varchar(20) DEFAULT NULL,
  `last_name` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password_hash` char(64) DEFAULT NULL,
  `portrait_image` blob DEFAULT NULL,
  `hourly_rate` decimal(9,2) DEFAULT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of accounts
-- ----------------------------
INSERT INTO `accounts` VALUES ('1', null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for `bugs`
-- ----------------------------
DROP TABLE IF EXISTS `bugs`;
CREATE TABLE `bugs` (
  `bug_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `date_reported` date NOT NULL,
  `summary` varchar(80) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `resolution` varchar(1000) DEFAULT NULL,
  `reported_by` bigint(20) unsigned DEFAULT NULL,
  `assigned_to` bigint(20) unsigned DEFAULT NULL,
  `verified_by` bigint(20) unsigned DEFAULT NULL,
  `status` varchar(20) DEFAULT 'NEW',
  `priority` varchar(20) DEFAULT NULL,
  `hours` decimal(9,2) DEFAULT NULL,
  PRIMARY KEY (`bug_id`),
  KEY `reported_by` (`reported_by`),
  KEY `assigned_to` (`assigned_to`),
  KEY `verified_by` (`verified_by`),
  KEY `status` (`status`),
  CONSTRAINT `bugs_ibfk_1` FOREIGN KEY (`reported_by`) REFERENCES `accounts` (`account_id`),
  CONSTRAINT `bugs_ibfk_2` FOREIGN KEY (`assigned_to`) REFERENCES `accounts` (`account_id`),
  CONSTRAINT `bugs_ibfk_3` FOREIGN KEY (`verified_by`) REFERENCES `accounts` (`account_id`),
  CONSTRAINT `bugs_ibfk_4` FOREIGN KEY (`status`) REFERENCES `bugstatus` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=5679 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of bugs
-- ----------------------------
INSERT INTO `bugs` VALUES ('1234', '2020-09-30', 'crash when I save', null, null, null, null, null, 'NEW', null, null);
INSERT INTO `bugs` VALUES ('2345', '2020-09-29', 'increase performance', null, null, null, null, null, 'NEW', null, null);
INSERT INTO `bugs` VALUES ('3456', '2020-09-28', 'screen goes blank', null, null, null, null, null, 'NEW', null, null);
INSERT INTO `bugs` VALUES ('5678', '2020-09-20', 'unknown conflict between products', null, null, null, null, null, 'NEW', null, null);

-- ----------------------------
-- Table structure for `bugsproducts`
-- ----------------------------
DROP TABLE IF EXISTS `bugsproducts`;
CREATE TABLE `bugsproducts` (
  `bug_id` bigint(20) unsigned NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`bug_id`,`product_id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `bugsproducts_ibfk_1` FOREIGN KEY (`bug_id`) REFERENCES `bugs` (`bug_id`),
  CONSTRAINT `bugsproducts_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of bugsproducts
-- ----------------------------
INSERT INTO `bugsproducts` VALUES ('1234', '1');
INSERT INTO `bugsproducts` VALUES ('1234', '3');
INSERT INTO `bugsproducts` VALUES ('3456', '2');
INSERT INTO `bugsproducts` VALUES ('5678', '1');
INSERT INTO `bugsproducts` VALUES ('5678', '3');

-- ----------------------------
-- Table structure for `bugstatus`
-- ----------------------------
DROP TABLE IF EXISTS `bugstatus`;
CREATE TABLE `bugstatus` (
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of bugstatus
-- ----------------------------
INSERT INTO `bugstatus` VALUES ('NEW');

-- ----------------------------
-- Table structure for `comments`
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `comment_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `bug_id` bigint(20) unsigned NOT NULL,
  `author` bigint(20) unsigned DEFAULT NULL,
  `comment_date` datetime NOT NULL,
  `comment` text NOT NULL,
  PRIMARY KEY (`comment_id`),
  KEY `bug_id` (`bug_id`),
  KEY `author` (`author`),
  CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`bug_id`) REFERENCES `bugs` (`bug_id`),
  CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`author`) REFERENCES `accounts` (`account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9877 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of comments
-- ----------------------------
INSERT INTO `comments` VALUES ('6789', '1234', null, '0000-00-00 00:00:00', 'It crashes!');
INSERT INTO `comments` VALUES ('9876', '2345', null, '0000-00-00 00:00:00', 'Great idea!');

-- ----------------------------
-- Table structure for `products`
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `product_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('1', 'Open RoundFile');
INSERT INTO `products` VALUES ('2', 'Visual TurboBuilder');
INSERT INTO `products` VALUES ('3', 'ReConsider');

-- ----------------------------
-- Table structure for `screenshots`
-- ----------------------------
DROP TABLE IF EXISTS `screenshots`;
CREATE TABLE `screenshots` (
  `bug_id` bigint(20) unsigned NOT NULL,
  `image_id` bigint(20) unsigned NOT NULL,
  `screenshot_image` blob DEFAULT NULL,
  `caption` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`bug_id`,`image_id`),
  CONSTRAINT `screenshots_ibfk_1` FOREIGN KEY (`bug_id`) REFERENCES `bugs` (`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of screenshots
-- ----------------------------

-- ----------------------------
-- Table structure for `tags`
-- ----------------------------
DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags` (
  `bug_id` bigint(20) unsigned NOT NULL,
  `tag` varchar(20) NOT NULL,
  PRIMARY KEY (`bug_id`,`tag`),
  CONSTRAINT `tags_ibfk_1` FOREIGN KEY (`bug_id`) REFERENCES `bugs` (`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tags
-- ----------------------------
